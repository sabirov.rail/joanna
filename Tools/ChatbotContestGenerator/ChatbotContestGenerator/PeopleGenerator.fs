﻿namespace ChatbotContestGenerator

module PeopleGenerator =

    open System
    open Furl

    let private splitLines (text:string) = Seq.toList (text.Split '\n')

    let private (|Contains|_|) (pattern:string) (str:string) =
        if (str.ToLower().Contains(pattern.ToLower())) then Some pattern
        else None

    let private pickRandomElems (items:string[]) (rnd:Random) =
        match items.Length with
        | 0 -> [|""|]
        | n ->         
            items
            |> Array.sortBy (fun _ -> rnd.Next())
            |> Array.take(rnd.Next(1, n))

    let private pickRandomResponsibilities allItems =
        let rnd = new Random(DateTime.Now.Millisecond)
        allItems
        |> List.map (fun (src, resp) -> (src, pickRandomElems resp rnd))

    let private replace (placeHolder:string) (source:string) content =
        source.Replace(placeHolder, content)

    let private concat separator elems =
        String.concat separator elems

    let private format elems placeholder source =
        elems
        |> concat ", "
        |> replace placeholder source

    let private formatUrl baseUrl (numberToGenerate:int) =
        replace "{count}" baseUrl (string numberToGenerate)

    let furlGet url =
        Furl.get url []

    // ---------------------------------------------------------------------------------------
    let private assignResponsibilitiesPerRole (person:string) =
        match person with
        | Contains "HR Manager" _ -> (person, [|"Relocations"; "Business trips"; "Events"; "Benefits"; "Medical care"|])
        | Contains "IT Support Specialist" _ -> (person, [|"Network monitoring"; "Hardware maintenance"; "Account creation"; "Laptop provisioning"|])
        | Contains "Team Manager" _ -> (person, [|"Expenses approval"; "Staffing"; "Performance reviews"; "Compensation reviews"; "Hiring"|])
        | Contains "Administrative Assistant" _ -> (person, [|"Expenses"; "Equipment purchasing"; "Building administration"; "Polish lessons"; "English lessons"; "First aid"|])
        | _ -> (person, [|""|])

    let public GeneratePeopleJson baseUrl numberToGenerate =
        formatUrl baseUrl numberToGenerate
        |> furlGet // Get the generated data from Mockaroo
        |> Furl.bodyText // Extract the Json content from the page
        |> splitLines // create a list of lines
        |> List.map assignResponsibilitiesPerRole // generate responsibilites for each line based on role
        |> pickRandomResponsibilities // pick a random set of responsibilites from the list of each person
        |> List.map (fun (src, resp) -> format resp "{responsibilities}" src) // insert the responsibilites in the lines
        |> concat "\n" // format back to Json