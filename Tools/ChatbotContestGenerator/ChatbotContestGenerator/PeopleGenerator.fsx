﻿#load "./Furl.fsx"

open System

let splitLines (text:string) = Seq.toList (text.Split '\n')

let (|Contains|_|) (pattern:string) (str:string) =
    if (str.ToLower().Contains(pattern.ToLower())) then Some pattern
    else None

let assignResponsibilitiesPerRole (person:string) =
    match person with
    | Contains "HR Manager" _ -> (person, ["Relocations"; "Business trips"; "Events"])
    | Contains "IT Support Specialist" _ -> (person, ["Network monitoring"; "Computer maintenance"])
    | Contains "Team Manager" _ -> (person, ["Expenses approval"; "Staffing"; "Performance reviews"])
    | Contains "Administrative Assistant" _ -> (person, ["Expenses"; "Equipment purchasing"])
    | _ -> (person, [""])

let replace (placeHolder:string) (source:string) content =
    source.Replace(placeHolder, content)

let concat separator (elems:string list) =
    String.concat separator elems

let format elems placeholder source =
    elems
    |> concat ", "
    |> replace placeholder source

// ---------------------------------------------------------------------------------------

Furl.get "https://www.mockaroo.com/f429de70/download?count=100&key=df449e20" [] 
|> Furl.bodyText // Extract the Json content from the page
|> splitLines // create a list of lines
|> List.map assignResponsibilitiesPerRole // generate responsibilites for each line based on role
|> List.map (fun (src, resp) -> format resp "{responsibilities}" src) // insert the responsibilites in the lines
|> concat "\n" // format back to Json

// ---------------------------------------------------------------------------------------

(*
assignResponsibilitiesPerRole "some Consultant here!"
assignResponsibilitiesPerRole "Hello I'm a QA specialist!"
assignResponsibilitiesPerRole "Hello I'm a team manager!"

replace "{placeholder}" "Hello, I'm {placeholder}!" "a robot"
concat ", " ["Expenses"; "Equipment purchasing"]
format ["One"; "Two"; "Three"] "{placeholder}" "F# is as easy as {placeholder}!"
*)