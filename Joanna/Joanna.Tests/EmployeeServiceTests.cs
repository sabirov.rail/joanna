﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using FluentAssertions;
using Joanna.Models;
using Joanna.Repositories;
using Joanna.Services;
using Newtonsoft.Json;
using NUnit.Framework;

namespace Joanna.Tests
{
    [TestFixture, Category("Integration")]
    public class EmployeeServiceTests
    {
        [Test]
        public void GetOne()
        {
            // Arrange
            DocumentDbRepository<Employee>.Initialize();
            var sut = new EmployeeService();
            // Act
            var actual = sut.GetMany("Jacqueline", "Stanley").Result.ToList();
            // Assert
            actual.Should().NotBeNull();
            actual.Should().HaveCount(1);
            actual[0].FirstName.Should().Be("Jacqueline");
            actual[0].LastName.Should().Be("Stanley");
            actual[0].Gender.Should().Be("Female");
        }

        [Test]
        public void GetAll()
        {
            // Arrange
            DocumentDbRepository<Employee>.Initialize();
            var sut = new EmployeeService();
            // Act
            var actual = sut.GetMany(employee => true).Result.ToList();
            // Assert
            actual.Should().NotBeNull();
            Assert.IsTrue(actual.Count == 103);
        }

        [Test]
        public void GetAllEmployeesWithResponsibilities()
        {
            // Arrange
            DocumentDbRepository<Employee>.Initialize();
            var sut = new EmployeeService();
            // Act
            var actual = sut.GetMany(e => e.Responsibilities != "").Result.ToList();
            // Assert
            actual.Should().NotBeNull();
            actual.Count.Should().BeGreaterThan(1);
            Assert.IsTrue(actual.All(e => !string.IsNullOrEmpty(e.Responsibilities)));
            // File.WriteAllText(@"c:\temp\empl.txt", string.Join(Environment.NewLine, actual.Select(e => e.ToString() + " " + e.Responsibilities)));            
        }

        [Test]
        public void GetAllConsultants()
        {
            // Arrange
            DocumentDbRepository<Employee>.Initialize();
            var sut = new EmployeeService();
            // Act
            var actual = sut.GetMany(e => e.Role.ToLower() == "consultant").Result;
            // Assert
            actual.Should().NotBeNull();
            foreach (var employee in actual)
            {
                employee.Role.ToLower().Should().Be("consultant");
            }
        }

        [Test]
        public void GetAllQAFromToronto()
        {
            // Arrange
            DocumentDbRepository<Employee>.Initialize();
            var sut = new EmployeeService();
            // Act
            var actual = sut.GetMany(e => e.Role.ToLower() == "qa specialist" && e.Location.ToLower() == "toronto").Result;
            // Assert
            actual.Should().NotBeNull();
            foreach (var employee in actual)
            {
                employee.Role.ToLower().Should().Be("qa specialist");
                employee.Location.ToLower().Should().Be("toronto");
            }
        }

        [Test]
        public void Get_ReturnNoResults()
        {
            // Arrange
            DocumentDbRepository<Employee>.Initialize();
            var sut = new EmployeeService();
            // Act
            var actual = sut.GetMany(e => e.Role.ToLower() == "qa specialist" && e.Responsibilities.ToLower().Contains("monitoring")).Result;
            // Assert
            actual.Should().NotBeNull();
            actual.Should().HaveCount(0, "because there is no such combination of role and responsibility");
        }

        [Test]
        public void GetByResponsibilityFromLocation()
        {
            // Arrange
            DocumentDbRepository<Employee>.Initialize();
            var sut = new EmployeeService();
            // Act
            var actual = sut.GetByResponsibility("monitoring", "New York").Result;
            // Assert
            actual.Should().NotBeNull();
            foreach (var employee in actual)
            {
                employee.Role.ToLower().Should().Be("it support specialist");
                employee.Location.ToLower().Should().Be("new york");
            }
        }

        [Test]
        public void GetByResponsibilityGlobally()
        {
            // Arrange
            DocumentDbRepository<Employee>.Initialize();
            var sut = new EmployeeService();
            // Act
            var actual = sut.GetByResponsibility("events").Result;
            // Assert
            actual.Should().NotBeNull();

            var roles = actual.Select(e => e.Role).Distinct().ToList();
            roles.Should().HaveCount(1);
            roles[0].ToLower().Should().Be("hr manager", "because only HR managers handle events");
            actual.Select(e => e.Location).Distinct().Count().Should().BeGreaterThan(1, "because several locations have a HR manager");
        }

        [Test]
        public void GetManagerOf()
        {
            // Arrange
            DocumentDbRepository<Employee>.Initialize();
            var sut = new EmployeeService();
            // Act
            var actual = sut.GetManagerOf("Stanislav Ivanov").Result;
            // Assert
            actual.Should().NotBeNull();
            actual.Should().HaveCount(2);
            var location = actual.Select(e => e.Location.ToLower()).Distinct().ToList();
            location.Should().HaveCount(1);
            location[0].ToLower().Should().Be("wrocław");
        }

        [Test]
        public void GetManagerBy()
        {
            // Arrange
            DocumentDbRepository<Employee>.Initialize();
            var sut = new EmployeeService();
            // Act
            var actual = sut.GetManagedBy("Phillip Willis").Result;
            // Assert
            actual.Should().NotBeNull();
            actual.Should().HaveCount(16);
            var location = actual.Select(e => e.Location.ToLower()).Distinct().ToList();
            location.Should().HaveCount(1);
            location[0].ToLower().Should().Be("raleigh");
        }

        [Test]
        public void TryGetUniqueEmployee_IncompleteIdentity()
        {
            // Arrange
            DocumentDbRepository<Employee>.Initialize();
            var sut = new EmployeeService();

            // Act & Assert
            Action tryGetUniqueEmployee = () =>
            {
                // Last name missing here.
                var actual = sut.TryGetUniqueEmployee("Sharon").Result;
            };

            tryGetUniqueEmployee
                .ShouldThrow<AggregateException>()
                .WithInnerException<BothFirstAndLastNamesRequiredException>();
        }

        [Test]
        public void TryGetUniqueEmployee_FirstAndLastNameInCorrectOrder()
        {
            // Arrange
            DocumentDbRepository<Employee>.Initialize();
            var sut = new EmployeeService();
            // Act
            var actual = sut.TryGetUniqueEmployee("Sharon Sanchez").Result;
            // Assert
            actual.Should().NotBeNull();
            actual.FirstName.Should().Be("Sharon");
            actual.LastName.Should().Be("Sanchez");
        }

        [Test]
        public void TryGetUniqueEmployee_FirstAndLastNameInInvertedOrder()
        {
            // Arrange
            DocumentDbRepository<Employee>.Initialize();
            var sut = new EmployeeService();
            // Act
            var actual = sut.TryGetUniqueEmployee("Sanchez Sharon").Result;
            // Assert
            actual.Should().NotBeNull();
            actual.FirstName.Should().Be("Sharon");
            actual.LastName.Should().Be("Sanchez");
        }

        [Test]
        public void TryGetUniqueEmployee_DuplicateEmployees()
        {
            // Arrange
            DocumentDbRepository<Employee>.Initialize();
            var sut = new EmployeeService();

            // Act & Assert
            Action tryGetUniqueEmployee = () =>
            {
                // Duplicate created for tests purposes
                var actual = sut.TryGetUniqueEmployee("Christina King").Result;
            };

            tryGetUniqueEmployee
                .ShouldThrow<AggregateException>()
                .WithInnerException<DuplicateEmployeeException>();
        }
        
        //        [Test] // Uncomment to run! Beware, it will upload data to the database!
        public void UploadPeopleJsonFile()
        {
            DocumentDbRepository<Employee>.Initialize();
            // Read in lines from file.
            var filePath = Path.Combine(TestContext.CurrentContext.TestDirectory, @"..\..\..\..\..\people.json");
            var employees = JsonConvert.DeserializeObject<List<Employee>>(File.ReadAllText(filePath));

            foreach (var employee in employees)
            {
                DocumentDbRepository<Employee>.CreateAsync(DbCollections.Employees, employee).Wait();
            }
        }
    }
}