﻿using FluentAssertions;
using Joanna.Models;
using Joanna.Repositories;
using Joanna.Services;
using NUnit.Framework;

namespace Joanna.Tests
{
    [TestFixture]
    class QuestionDbServiceTests
    {
        [Test]
        public void CreateAndSaveNew_CorrectlySaved()
        {
            // Arrange
            DocumentDbRepository<Question>.Initialize();
            var sut = new QuestionDbService();
            // Act
            var actual = sut.CreateAndSaveNewQuestion("Where is the bathroom?", "Michael Jackson").Result;
            // Assert
            actual.Should().NotBeNull();
            actual.WasAnswered.Should().BeFalse();
            actual.Text.Should().Be("Where is the bathroom?");
            actual.AskedBy.Should().Be("Michael Jackson");
            actual.id.Should().NotBeEmpty();
            actual.Answer.Should().BeEmpty();
        }

        [Test]
        public void GetAllUnanswered()
        {
            // Arrange
            DocumentDbRepository<Question>.Initialize();
            var sut = new QuestionDbService();
            // Act
            var actual = sut.GetAllUnanswered().Result;
            // Assert
            actual.Should().NotBeNull();
            actual.Should().HaveCount(i => i > 0);
        }

        [Test]
        public void GetQuestionById()
        {
            // Arrange
            DocumentDbRepository<Question>.Initialize();
            var sut = new QuestionDbService();
            // Act
            var actual = sut.GetQuestionById("c938af48-18ea-4823-937c-a6a5258ae3aa").Result;
            // Assert
            actual.Should().NotBeNull();
        }
        
        [Test]
        public void AnswerQuestionScenario()
        {
            // Arrange
            DocumentDbRepository<Question>.Initialize();
            var sut = new QuestionDbService();
            // Act

            var newQuestion = sut.CreateAndSaveNewQuestion("Hello, it is me you're looking for?", "Lionel Richie").Result;
            var answeredQuestion = sut.AnswerQuestion(newQuestion, "Get out of the way, you fool!", "Donald Trump").Result;
            // Assert
            answeredQuestion.Should().NotBeNull();
            answeredQuestion.AnsweredBy.Should().Be("Donald Trump");
            answeredQuestion.Answer.Should().Be("Get out of the way, you fool!");
            answeredQuestion.WasAnswered.Should().BeTrue();
        }

        //[Test] // Use this method to upload new questions to the DB.
        public void UploadNewQuestion()
        {
            DocumentDbRepository<Question>.Initialize();
            var sut = new QuestionDbService();
            var actual = sut.CreateAndSaveNewQuestion("Where can I buy a good kebab in Toronto?", "Angela Merkel").Result;
        }
    }
}