﻿using System;

namespace Joanna.Services
{
    public interface IShortAnswerService
    {
        string Get();
    }

    public class ShortAnswerService : IShortAnswerService
    {
        private readonly Random random = new Random();

        private readonly string[] shortAnswers = new[]
           {
                "Tell me about it!",
                "I know what you mean!",
                "I told you so!",
                "I couldn't agree more.",
                "That's really inspiring! Tell me more.",
                "Did you really say that?",
                "No way!?",
                "Sounds good!"
            };

        public string Get()
        {
            int index = random.Next(0, shortAnswers.Length);
            return shortAnswers[index];
        }
    }
}