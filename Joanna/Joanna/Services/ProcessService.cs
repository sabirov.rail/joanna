﻿using Joanna.Models;
using Joanna.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Linq.Expressions;

namespace Joanna.Services
{
    public interface IProcessService
    {
        /// <summary>
        /// Returns a single process by its name in specified location
        /// </summary>
        /// <param name="name"></param>
        /// <param name="location"></param>
        /// <returns></returns>
        Task<Process> GetProcess(string name, string location);

        /// <summary>
        /// Lists all the processes for a particular location
        /// </summary>
        /// <param name="location"></param>
        /// <returns></returns>
        Task<IEnumerable<Process>> GetProcessesByLocation(string location);

        /// <summary>
        /// Lists all the processes for a particular location in less strict manner (partial match)
        /// </summary>
        /// <param name="location"></param>
        /// <returns></returns>
        Task<IEnumerable<Process>> GetProcessesByLocationLike(string location);

        /// <summary>
        /// Looks for processes in less strict manner (case insensitive)
        /// </summary>
        /// <param name="name"></param>
        /// <param name="location"></param>
        /// <returns></returns>
        Task<IEnumerable<Process>> GetProcessesLike(string name, string location);
    }

    public class ProcessService : IProcessService
    {        
        public async Task<Process> GetProcess(string name, string location)
        {
            var lowerName = name.ToLower();
            var lowerLocation = location.ToLower();
            var processes = await GetMany(p => p.Name.ToLower() == lowerName && p.Location.ToLower() == lowerLocation);
            return processes.FirstOrDefault();
        }

        public async Task<IEnumerable<Process>> GetProcessesByLocation(string location)
        {
            var lowerLocation = location.ToLower();
            var processes = await GetMany(p => p.Location.ToLower() == lowerLocation);
            return processes;
        }
        
        public async Task<IEnumerable<Process>> GetProcessesByLocationLike(string location)
        {
            var lowerLocation = location.ToLower();
            var processes = await GetMany(p => p.Location.ToLower().Contains(lowerLocation));
            return processes;
        }

        public async Task<IEnumerable<Process>> GetProcessesLike(string name, string location)
        {
            var lowerName = name.ToLower();
            var lowerLocation = location.ToLower();
            var processes = await GetMany(p => p.Name.ToLower().Contains(lowerName) && p.Location.ToLower().Contains(lowerLocation));
            return processes;
        }

        public async Task<IEnumerable<Process>> GetMany(Expression<Func<Process, bool>> filter)
        {
            var processes = await DocumentDbRepository<Process>.GetManyAsync(DbCollections.Processes, filter);
            return processes;
        }
    }
}