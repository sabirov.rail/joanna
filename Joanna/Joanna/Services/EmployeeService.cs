﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Joanna.Models;
using Joanna.Repositories;

namespace Joanna.Services
{
    public interface IEmployeeService
    {
        Task<Employee> TryGetUniqueEmployee(string fullName);
        Task<Employee> TryGetUniqueEmployeeById(Guid id);
        Task<IEnumerable<Employee>> GetMany(string firstName, string lastName);
        Task<IEnumerable<Employee>> GetByResponsibility(string responsibility, string location = null);
        Task<IEnumerable<Employee>> GetManagerOf(string fullName);
        Task<IEnumerable<Employee>> GetManagedBy(string fullName);
        Task<IEnumerable<Employee>> GetMany(Expression<Func<Employee, bool>> filter);
        Task<Employee> GetPersonByLocationAndPersonName(string location, string personNameOrSurnameOrBoth);
        Task<IEnumerable<Employee>> GetPersonsByLocationAndRole(string location, string role);
        Task<IEnumerable<string>> GetResponsibilitiesByRole(string role);
        Task<string> GetResponsibilitiesByPersonName(string name);
    }

    public class EmployeeService : IEmployeeService
    {
        /// <summary>
        /// Try to retrieve a unique employee from the DB.
        /// <exception cref="DuplicateEmployeeException"> Thrown if more than one employee with given full name was found. </exception>
        /// <exception cref="BothFirstAndLastNamesRequiredException"> Thrown if the full name didn't contain both first and last names. </exception>
        /// </summary>
        public async Task<Employee> TryGetUniqueEmployee(string fullName)
        {
            var identity = TryGetIdentity(fullName);
            
            IEnumerable<Employee> results;

            results = await GetMany(identity.PotentialFirstName, identity.PotentialLastName);
            if (!results.Any())
            {
                // Try again but invert first name and last name
                results = await GetMany(identity.PotentialLastName, identity.PotentialFirstName);
            }

            var employees = results.ToArray();
            if (!employees.Any())
                return null;

            if (employees.Count() > 1)
                throw new DuplicateEmployeeException(fullName, employees);
            
            return employees[0];
        }

        /// <summary>
        /// Try to retrieve a unique employee from the DB, based on his ID.
        /// </summary>
        public async Task<Employee> TryGetUniqueEmployeeById(Guid id)
        {
            var employee = await DocumentDbRepository<Employee>.GetAsync(DbCollections.Employees, id.ToString());
            return employee;
        }
        
        public async Task<IEnumerable<Employee>> GetMany(string firstName, string lastName)
        {
            var employees = await DocumentDbRepository<Employee>.GetManyAsync(DbCollections.Employees, e => e.FirstName.ToLower().Equals(firstName.ToLower()) && e.LastName.ToLower().Equals(lastName.ToLower()));
            return employees;
        }

        /// <summary>
        /// Answer the 'Who is responsible for?' question. Can filter per location.
        /// </summary>
        public async Task<IEnumerable<Employee>> GetByResponsibility(string responsibility, string location = null)
        {
            Expression<Func<Employee, bool>> query;

            if (String.IsNullOrEmpty(location))
            {
                query = e => e.Responsibilities.ToLower().Contains(responsibility.ToLower());
            }
            else
            {
                query = e => e.Responsibilities.ToLower().Contains(responsibility.ToLower()) && e.Location.ToLower() == location.ToLower();
            }

            var employees = await DocumentDbRepository<Employee>.GetManyAsync(DbCollections.Employees, query);
            return employees;
        }

        /// <summary>
        /// Answer the 'Whos is my manager?' question. Automatically filters based on location. Several employees can be returned.
        /// Can throw the same exceptions as <see cref="TryGetUniqueEmployee"/>./>
        /// </summary>
        public async Task<IEnumerable<Employee>> GetManagerOf(string fullName)
        {
            var employee = await TryGetUniqueEmployee(fullName);
            var managers = await GetMany(e => e.Role.ToLower() == "team manager" && e.Location.ToLower() == employee.Location.ToLower());
            return managers;
        }

        /// <summary>
        /// Answer the 'Whos are employees managed by 'X'?' question. Automatically filters based on location. Excludes other team managers from the results.
        /// Can throw the same exceptions as <see cref="TryGetUniqueEmployee"/>./>
        /// </summary>
        public async Task<IEnumerable<Employee>> GetManagedBy(string fullName)
        {
            var employee = await TryGetUniqueEmployee(fullName);
            var managedBy = await GetMany(e => e.Role.ToLower() != "team manager" && e.Location.ToLower() == employee.Location.ToLower());
            return managedBy;
        }

        /// <summary>
        /// Answer any advanced query on employees.
        /// Make sure your query only contains simple instructions, otherwise they won't be convertable to the underlying querying language.
        /// Make sure to use 'ToLower()' to make case-insensitve comparisons if needed.
        /// </summary>
        public async Task<IEnumerable<Employee>> GetMany(Expression<Func<Employee, bool>> filter)
        {
            var employees = await DocumentDbRepository<Employee>.GetManyAsync(DbCollections.Employees, filter);
            return employees;
        }

        private struct Identity
        {
            public string PotentialFirstName;
            public string PotentialLastName;
        }

        private Identity TryGetIdentity(string fullName)
        {
            var split = fullName.Split(' ');
            if (split.Length < 2)
                throw new BothFirstAndLastNamesRequiredException(fullName);

            // ignore the case when there's more than 2 items...
            return new Identity() { PotentialFirstName = split[0], PotentialLastName = split[1] };
        }

        public async Task<Employee> GetPersonByLocationAndPersonName(string location, string personNameOrSurnameOrBoth)
        {
            var identity = await TryGetUniqueEmployee(personNameOrSurnameOrBoth);
            var users = await GetMany(x => x.Location == location && x.LastName == identity.LastName && x.FirstName == identity.FirstName);
            return users.SingleOrDefault();
        }

        public async Task<IEnumerable<Employee>> GetPersonsByLocationAndRole(string location, string role)
        {
            return await GetMany(x => x.Location == location && x.Role == role);
        }

        public async Task<IEnumerable<string>> GetResponsibilitiesByRole(string role)
        {
            var users = await GetMany(x => x.Role == role);
            if (!users.Any()) return null;
            return users.Where(x => !string.IsNullOrWhiteSpace(x.Responsibilities)).Select(x => x.Responsibilities);
        }

        public async Task<string> GetResponsibilitiesByPersonName(string name)
        {
            var employee = await TryGetUniqueEmployee(name);
            return employee?.Responsibilities;
        }
    }

    public class DuplicateEmployeeException : Exception
    {
        public string Name { get; private set; }
        public IEnumerable<Employee> DuplicatEmployees { get; private set; }

        public DuplicateEmployeeException(string name, IEnumerable<Employee> duplicateEmployees)
        {
            Name = name;
            DuplicatEmployees = duplicateEmployees;
        }
    }

    public class BothFirstAndLastNamesRequiredException : Exception
    {
        public string Name { get; private set; }

        public BothFirstAndLastNamesRequiredException(string name) : base($"I need your name and surname.")
        {
            Name = name;
        }
    }

    public class EmployeeNotFoundException : Exception
    {
        public string Name { get; private set; }

        public EmployeeNotFoundException(string name) : base($"Unfortunately you are not registered.")
        {
            Name = name;
        }
    }
}