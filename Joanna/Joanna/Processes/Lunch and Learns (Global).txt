﻿Lunch and Learn preparation process

1. Confirm the date of L&L with the presenter and/or with local director of engineering.
2. Ask the presenter for the presentation overview.
3. Review sessions with presenters and help them assign the level
4. Advertise sessions with expected level (entry, intermediate, advanced, hardcore) 
5. Send an invitation at least 1 week prior the meeting.
6. Book the meeting rooms from at least 15 minutes prior to the session to allow for technical setup.
7. Set the webex meeting to start 15 minutes prior to the presentation so we can join before the scheduled time.
8. Appoint leaders in each location to be responsible for the technical details to ensure things are ready.
9. Get the presenters to practice using WebEx briefly the day before to ensure there are no surprises.
10. Find volunteers to pick up food onsite.