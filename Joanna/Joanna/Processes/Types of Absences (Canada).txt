﻿In Canada we have the following types of absences:

1. Holidays
2. Vacation, Sick Days
3. Leave of Absence
4. Bereavement
5. Jury Duty