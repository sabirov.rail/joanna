﻿using Microsoft.Bot.Builder.FormFlow;
using System;

namespace Joanna.Models
{
    [Serializable]
    public class UserAuth
    {
        [Prompt("Could you please identify yourself? Enter your first name and last name only.")]
        public string FullName;

        public static IForm<UserAuth> BuildForm()
        {
            return new FormBuilder<UserAuth>()
                   .Message("Hello there! I'm Joanna, your Smart Office Assistant!")
                   .Field(nameof(FullName))
                   .Build();
        }
    }
}