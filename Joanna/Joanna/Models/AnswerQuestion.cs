﻿using Microsoft.Bot.Builder.FormFlow;
using System;

namespace Joanna.Models
{
    [Serializable]
    public class AnswerQuestion
    {
        public AnswerQuestion(string id)
        {
            Id = id;
        }

        public string Answer;
        public readonly string Id;

        public static IForm<AnswerQuestion> BuildForm()
        {
            return new FormBuilder<AnswerQuestion>()
                   .Field(nameof(Answer))
                   .Build();
        }
    }
}