﻿using System;
using Newtonsoft.Json;

namespace Joanna.Models
{
    [Serializable]
    public class Employee
    {
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public string Location { get; set; }
        public string Role { get; set; }
        public string Responsibilities { get; set; }

        private string _email;
        public string Email => GetEmail();

        public string FullName { get
            {
                return FirstName + " " + LastName;
            }
        }

        private string GetEmail()
        {
            if (_email == null)
                _email = FirstName + "." + LastName + "@infusion.com";

            return _email;
        }

        public override string ToString()
        {
            return $"{FirstName} {LastName}, {Role}, {Location} ({Email})";
        }
    }

    [Serializable]
    public class Question
    {
        public string id { get; set; }
        [JsonProperty("text")]
        public string Text { get; set; }
        [JsonProperty("askedBy")]
        public string AskedBy { get; set; }
        [JsonProperty("askedOn")]
        public DateTime AskedOn { get; set; }
        [JsonProperty("wasAnswered")]
        public bool WasAnswered { get; set; }
        [JsonProperty("answer")]
        public string Answer { get; set; }
        [JsonProperty("answeredBy")]
        public string AnsweredBy { get; set; }
        [JsonProperty("answeredOn")]
        public DateTime? AnsweredOn { get; set; }

        public override string ToString()
        {
            return $"{Text} - {Answer}";
        }
    }

    [Serializable]
    public class Process
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Location { get; set; }
        public string Responsible { get; set; } // decided to store name + surname here

        public override string ToString()
        {
            var twoNewLines = Environment.NewLine + Environment.NewLine;
            return $"{Name} in {Location}, responsible: {Responsible}{twoNewLines}{Description}";
        }

        public string ToShortString()
        {
            return $"{Name} in {Location}, responsible: {Responsible}";
        }
    }
}