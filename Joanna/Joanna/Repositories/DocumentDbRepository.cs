﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using Microsoft.Azure.Documents.Linq;

namespace Joanna.Repositories
{
    public static class DbCollections
    {
        public static readonly string Database = ConfigurationManager.AppSettings["dbName"];
        public static readonly string Employees = ConfigurationManager.AppSettings["dbEmployeesCollection"];
        public static readonly string Questions = ConfigurationManager.AppSettings["dbQuestionsCollection"];
        public static readonly string Processes = ConfigurationManager.AppSettings["dbProcessesCollection"];
    }

    public static class DocumentDbRepository<T> where T : class
    {
        private static DocumentClient _client;

        public static async Task<T> GetAsync(string collectionId, string id)
        {
            try
            {
                Document document = await _client.ReadDocumentAsync(UriFactory.CreateDocumentUri(DbCollections.Database, collectionId, id));
                return (T) (dynamic) document;
            }
            catch (DocumentClientException e)
            {
                if (e.StatusCode == System.Net.HttpStatusCode.NotFound)
                {
                    return null;
                }
                else
                {
                    throw;
                }
            }
        }

        public static async Task<IEnumerable<T>> GetManyAsync(string collectionId, Expression<Func<T, bool>> predicate)
        {
            IDocumentQuery<T> query = _client.CreateDocumentQuery<T>(
                UriFactory.CreateDocumentCollectionUri(DbCollections.Database, collectionId),
                new FeedOptions { MaxItemCount = -1 })
                .Where(predicate)
                .AsDocumentQuery();

            List<T> results = new List<T>();
            while (query.HasMoreResults)
            {
                results.AddRange(await query.ExecuteNextAsync<T>());
            }

            return results;
        }

        public static async Task<Document> CreateAsync(string collectionId, T document)
        {
            return await _client.CreateDocumentAsync(UriFactory.CreateDocumentCollectionUri(DbCollections.Database, collectionId), document);
        }

        public static async Task<Document> UpdateAsync(string collectionId, string documentId, T document)
        {
            return await _client.ReplaceDocumentAsync(UriFactory.CreateDocumentUri(DbCollections.Database, collectionId, documentId), document);
        }

        public static async Task DeleteAsync(string documentId, string collectionId)
        {
            await _client.DeleteDocumentAsync(UriFactory.CreateDocumentUri(DbCollections.Database, collectionId, documentId));
        }

        public static void Initialize()
        {
            _client = new DocumentClient(new Uri(ConfigurationManager.AppSettings["dbEndpoint"]), ConfigurationManager.AppSettings["dbAuthKey"]);

            Thread.Sleep(100);
            InitDatabase().Wait();
            InitCollection(DbCollections.Employees).Wait();
            InitCollection(DbCollections.Questions).Wait();
            InitCollection(DbCollections.Processes).Wait();
        }

        private static async Task InitCollection(string collectionId)
        {
            try
            {
                await _client.ReadDocumentCollectionAsync(UriFactory.CreateDocumentCollectionUri(DbCollections.Database, collectionId));
            }
            catch (DocumentClientException ex)
            {
                Console.WriteLine(ex);
            }
        }

        private static async Task InitDatabase()
        {
            try
            {
                await _client.ReadDatabaseAsync(UriFactory.CreateDatabaseUri(DbCollections.Database));
            }
            catch (DocumentClientException ex)
            {
                Console.WriteLine(ex);
            }
        }
    }
}