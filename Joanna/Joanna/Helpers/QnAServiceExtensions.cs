﻿using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Joanna.Helpers
{
    public static class QnAServiceExtensions
    {
        private const string baseRequestUri = "https://westus.api.cognitive.microsoft.com/qnamaker/v2.0/knowledgebases/";

        public static async Task<bool> UpdateQnAService(string subscriptionKey, string knowledgeBaseId, string question, string answer)
        {
            var requestUri = baseRequestUri + knowledgeBaseId;

            var method = new HttpMethod("PATCH");

            var request = new HttpRequestMessage(method, requestUri)
            {
                Content = new StringContent("{\"add\":{\"qnaPairs\":[{\"answer\": \"" + answer + "\",\"question\": \"" + question + "\"}]}}", Encoding.UTF8,
                                    "application/json")
            };

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", subscriptionKey);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = new HttpResponseMessage();

                response = await client.SendAsync(request);
                return response.IsSuccessStatusCode;
            }
        }

        public static async Task<bool> PublishQnAService(string subscriptionKey, string knowledgeBaseId)
        {
            var requestUri = baseRequestUri + knowledgeBaseId;

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", subscriptionKey);
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = new HttpResponseMessage();

                var emptyContent = new StringContent(string.Empty);
                response = await client.PutAsync(requestUri, emptyContent);

                return response.IsSuccessStatusCode;
            }
        }
    }
}