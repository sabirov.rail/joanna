﻿using System;

namespace Joanna.Helpers
{
    public static class StringExtensions
    {
        public static bool EqualsCI(this string source, string other)
        {
            return source.Equals(other, StringComparison.InvariantCultureIgnoreCase);
        }

        public static bool AreEqualCI(string source, string other)
        {
            return source.Equals(other, StringComparison.InvariantCultureIgnoreCase);
        }

        public static bool IsEmpty(this string source)
        {
            return String.IsNullOrWhiteSpace(source);
        }
    }
}