﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Joanna.Helpers
{
    public static class ModelCollectionExtensions
    {
        public static async Task ProcessAsync<T>(this IEnumerable<T> employees, Func<Task> none, Func<T, Task> onlyOne, Func<IEnumerable<T>, Task> many)
        {
            var employeesList = employees.ToArray();
            var employeesCount = employeesList.Length;

            if (employeesCount == 0)
            {
                await none();
            }
            else if (employeesCount == 1)
            {
                await onlyOne(employeesList[0]);
            }
            else
            {
                await many(employeesList);
            }
        }
    }
}