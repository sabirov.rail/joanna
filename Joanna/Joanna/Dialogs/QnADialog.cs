﻿using System;
using System.Configuration;
using System.Threading.Tasks;
using Microsoft.Bot.Builder.Dialogs;
using QnAMakerDialog;
using Joanna.Services;
using Microsoft.Bot.Builder.Internals.Fibers;
using Joanna.Helpers;

namespace Joanna.Dialogs
{
    [Serializable]
    public class QnADialog : QnAMakerDialog<Guid>
    {
        private readonly IQuestionDbService questionDbService;
        private readonly IEmployeeService employeeService;
        private readonly IShortAnswerService shortAnswerService;

        public QnADialog(IQuestionDbService questionDbService, IEmployeeService employeeService, IShortAnswerService shortAnswerService) : base()
        {
            SubscriptionKey = ConfigurationManager.AppSettings["QnASubscriptionKey"];
            KnowledgeBaseId = ConfigurationManager.AppSettings["QnAKnowledgebaseId"];
            SetField.NotNull(out this.questionDbService, nameof(questionDbService), questionDbService);
            SetField.NotNull(out this.employeeService, nameof(employeeService), employeeService);
            SetField.NotNull(out this.shortAnswerService, nameof(shortAnswerService), shortAnswerService);
        }
        public override async Task NoMatchHandler(IDialogContext context, string originalQueryText)
        {
            originalQueryText = originalQueryText.Trim();
            await RegisterQuestion(context, originalQueryText);

            context.Done(Guid.NewGuid());
        }

        private async Task RegisterQuestion(IDialogContext context, string originalQueryText)
        {
            if (originalQueryText.Split(new[] { ' ' }).Length > 1 && originalQueryText.EndsWith("?"))
            {
                await context.PostAsync($"Can't answer that sorry. Let me write it down to our knowledge base. Maybe someone will know the answer?");

                await questionDbService.CreateAndSaveNewQuestion(originalQueryText, (await context.GetCurrentUser(employeeService)).FullName);

                await context.PostAsync($"If you want to contribute, just type 'I want to answer a question' or 'I want to contribute'");
            }
            else
            {
                var answer = this.shortAnswerService.Get();
                await context.PostAsync($"{answer}");
            }
        }

        public override async Task DefaultMatchHandler(IDialogContext context, string originalQueryText, QnAMakerResult result)
        {
            if (result.Score > 90)
            {
                await context.PostAsync(result.Answer);
            }
            else
            {
                await RegisterQuestion(context, originalQueryText);
            }
                
            context.Done(Guid.Empty);
        }
    }
}