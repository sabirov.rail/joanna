﻿namespace Joanna.Dialogs
{
    internal static class Constants
    {
        public const string PersonKey = "PersonKey";
        public const string TriedToAnswerQuestionsKey = "TriedToAnswerQuestionsKey";

        public const string Entity_Responsibility = "responsibility";
        public const string Entity_Location = "location";
        public const string Entity_Person = "person";
        public const string Entity_Process = "process";
    }
}