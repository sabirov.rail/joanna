﻿using System;
using System.Configuration;
using System.Threading;
using System.Threading.Tasks;
using AuthBot;
using AuthBot.Dialogs;
using AuthBot.Models;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using Joanna.Models;
using Microsoft.Bot.Builder.FormFlow;
using Joanna.Services;
using Microsoft.Bot.Builder.Internals.Fibers;

namespace Joanna.Dialogs
{
    /// <summary>
    /// Should be splitted in 3 dialogs: RootDialog, AuthByFullNameDialog and AuthByADDialog
    /// </summary>
    [Serializable]
    public class UserAuthenticationDialog : IDialog<object>
    {
        public const string LogoutMessage = "logout";

        public const string AuthenticationTypeSettingName = "AuthenticationType";

        private static readonly Lazy<AuthenticationType> AuthenticationType = new Lazy<AuthenticationType>(
            () => (AuthenticationType) Enum.Parse(
                typeof(AuthenticationType),
                ConfigurationManager.AppSettings[AuthenticationTypeSettingName]));

        private static readonly Lazy<string> ResourceId = new Lazy<string>(
            () => ConfigurationManager.AppSettings["ActiveDirectory.ResourceId"]);

        private readonly LuisDialog<object> _luisDialog;
        private readonly IEmployeeService _employeeService;
        private readonly IQuestionDbService _questionDbService;
        private readonly IAnswerQuestionsDialog _answerQuestionsDialog;

        public UserAuthenticationDialog(LuisDialog<object> luisDialog, IEmployeeService employeeService,
            IQuestionDbService questionDbService, IAnswerQuestionsDialog answerQuestionsDialog)
        {
            SetField.NotNull(out _luisDialog, nameof(luisDialog), luisDialog);
            SetField.NotNull(out _employeeService, nameof(employeeService), employeeService);
            SetField.NotNull(out _questionDbService, nameof(questionDbService), questionDbService);
            SetField.NotNull(out _answerQuestionsDialog, nameof(answerQuestionsDialog), answerQuestionsDialog);
        }

        public async Task StartAsync(IDialogContext context)
        {
            context.Wait(MessageReceivedAsync);
        }

        public virtual async Task MessageReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> item)
        {
            var message = await item;
            if (IsLoggedIn(context))
            {
                if ((message.Text.Contains("want to answer") && message.Text.Contains("question")) ||
                    message.Text.Contains("want to contribute"))
                {
                    await context.Forward(_answerQuestionsDialog, ResumeAfterAnsweredQuestion, message, CancellationToken.None);
                }
                else if (ShouldLogout(message))
                {
                    await Logout(context);
                }
                else
                {
                    await context.Forward(_luisDialog, ResumeAfterLuisDialog, message, CancellationToken.None);
                }
            }
            else
            {
                await Login(context, message);
            }
        }

        private async Task ResumeAfterAnsweredQuestion(IDialogContext context, IAwaitable<object> result)
        {
            await result;
            context.Wait(this.MessageReceivedAsync);
        }

        private bool IsLoggedIn(IDialogContext context)
        {
            Guid personKey;

            return
                context.UserData.TryGetValue(Constants.PersonKey, out personKey) &&
                personKey != Guid.Empty;
        }

        private bool ShouldLogout(IMessageActivity message)
        {
            return LogoutMessage.Equals(message.Text, StringComparison.CurrentCultureIgnoreCase);
        }

        private void SetAuthenticationData(IDialogContext context, Employee user)
        {
            context.UserData.SetValue(Constants.PersonKey, new Guid(user.Id));
        }

        private void ClearAuthenticationData(IDialogContext context)
        {
            context.UserData.SetValue(Constants.PersonKey, Guid.Empty);
        }

        private async Task Login(IDialogContext context, IMessageActivity message)
        {
            switch (AuthenticationType.Value)
            {
                case Dialogs.AuthenticationType.AzureActiveDirectory:
                    await AzureLogin(context, message);
                    break;

                case Dialogs.AuthenticationType.Manual:
                    ManualLogin(context);
                    break;
            }
        }

        private async Task Login(IDialogContext context, string fullName)
        {
            try
            {
                var user = await _employeeService.TryGetUniqueEmployee(fullName);
                if (user != null)
                {
                    SetAuthenticationData(context, user);

                    await context.PostAsync(
                        $"Oh hello {user.FirstName}! I didn't recognize you at first!\n\nHow may I be of assistance today?");
                    context.Call(_luisDialog, this.ResumeAfterLuisDialog);
                }
                else
                {
                    await context.PostAsync(
                        $"mmm, sorry {fullName}, I don't think we know each other... You may want to talk to a manager first.");
                    context.Wait(MessageReceivedAsync);
                }
            }
            catch (DuplicateEmployeeException ex)
            {
                await context.PostAsync(
                    $"Oh no! I found several employees under the name {ex.Name}... I'm afraid I won't be able to help you today!");
            }
            catch (BothFirstAndLastNamesRequiredException)
            {
                await context.PostAsync(
                    $"It seems you didn't give me enough information. I need both your first and last names!");
            }
        }

        private async Task Logout(IDialogContext context)
        {
            ClearAuthenticationData(context);

            switch (AuthenticationType.Value)
            {
                case Dialogs.AuthenticationType.AzureActiveDirectory:
                    await AzureLogout(context);
                    break;

                case Dialogs.AuthenticationType.Manual:
                    await ManualLogout(context);
                    break;
            }
        }

        private async Task AzureLogin(IDialogContext context, IMessageActivity message)
        {
            await context.Forward(
                new AzureAuthDialog(ResourceId.Value),
                ResumeAfterAzureAuthentication,
                message,
                CancellationToken.None);
        }

        private async Task AzureLogout(IDialogContext context)
        {
            await context.Logout();
        }

        private void ManualLogin(IDialogContext context)
        {
            BuildFormDelegate<UserAuth> authDelegate = UserAuth.BuildForm;

            var authForm = new FormDialog<UserAuth>(new UserAuth(), authDelegate, FormOptions.PromptInStart);
            context.Call(authForm, ResumeAfterManualAuthentication);
        }

        private async Task ManualLogout(IDialogContext context)
        {
            ClearAuthenticationData(context);

            await context.PostAsync("You have been successfully signed out");
        }

        private async Task ResumeAfterAzureAuthentication(IDialogContext context, IAwaitable<string> result)
        {
            try
            {
                var message = await result;
            }
            catch (OperationCanceledException)
            {
                await context.PostAsync("Unfortunately the authentication process finished unsuccessfully.");
                return;
            }

            AuthResult authResult;

            if (context.UserData.TryGetValue(ContextConstants.AuthResultKey, out authResult))
            {
                await Login(context, authResult.UserName);
            }
            else
            {
                await context.PostAsync("Unfortunately I cannot find your user profile in the repository.");
            }
        }

        private async Task ResumeAfterManualAuthentication(IDialogContext context, IAwaitable<UserAuth> result)
        {
            UserAuth authdata;
            try
            {
                authdata = await result;
            }
            catch (OperationCanceledException)
            {
                await context.PostAsync("Unfortunately I need to know your name and surname!");
                return;
            }

            if (authdata == null)
            {
                await context.PostAsync("Unfortunately I need to know your name and surname!");
            }
            else
            {
                await Login(context, authdata.FullName);
            }
        }

        private async Task ResumeAfterLuisDialog(IDialogContext context, IAwaitable<object> result)
        {
            var res = await result;
            context.Wait(this.MessageReceivedAsync);
        }
    }
}