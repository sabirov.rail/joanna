People & roles
==============================

WhoIsResponsibleFor	`(Person, Location?) -> Person`
---------------------
 - Who handles payroll?
 - Who is in charge of relocations in Wrocław?
 - Who can I talk to about my taxes?
 - Who is responsible for sales?
 - Is there anyone i can talk about Human Resources?
 - I want to find a person who knows about taxes
 - Anyone knows something about taxes in Toronto?

WhatIsRoleOf `(Person, Location?) -> Role`
------------------------------------------------	
 - What is Roger's role within the organisation?
 - What is John's position in Toronto?
 - What does John Rambo do?

GetPersonsWithRoleInRelationTo `(Person, Role, Location?) -> List<Person>`
------------------------------------------------------------	
 - Who is my manager? (here 'my' has to be translated to the current user)
 - What's the name of Tomek's team leader?
 - Who does Rachel report to? (might be too complex, as 'report to' implies 'manager')
 - Who works under Bob in Wrocław? (might be too complex, as 'work under' implies for 'subordinates')

GetAllWithRole `(Role, Location?) -> List<Person>`
------------------------------------------------------------		
 - Who is a consultant in Wrocław?
 - Give me the list of team leaders in Toronto.
 - Can you show me the list of all designers working for Infusion?
 - List all managers

WhatAreResponsibilitiesForRole	`Role -> List<Responsibility>`
------------------------------------------------------------	
 - What does a manager do?
 - List all responsibilities of a consultant.
 - What is sales resposible of?

WhatAreResponsibilitiesOf `Person -> List<Responsibility>`
------------------------------------------------------------	
 - What are my responsibilities? (here 'my' has to be translated to the current user)
 - What does Justyna do?
 - What are Donald's duties?

Room bookings
=============

DisplayBookingsFor
------------------
 - Show me bookings for Colossus on Friday.
 - When is Colossus busy today?
 - Display bookings for Wrocław tomorrow.

DisplayFreeSlotsFor
-------------------
 - When is room 404 available today?
 - Display all free slots for Colossus on Thursday.
 - Show me rooms available from 3pm to 5pm tomorrow in Kraków.
 - Show me free rooms in London.

DisplayRoomsPerLocation
-----------------------
 - What is the list of rooms in Wrocław?
 - Show free rooms in London?

TryBookRoom
-----------
 - Please book Colossus today from 11am to 12pm.
 - Can you book room 404 for Tony tomorrow?


Query process
==================

DisplayProcess
--------------
 - Where can I find information about relocation?
 - Show me the equipment purchasing process for Wrocław.

WhoIsResponsibleForProcess
--------------------------
 - Who can I talk to regarding expenses in Toronto?
 - Show me the contact person for relocations.

GetListOfProcesses
------------------
 - Display the whole list of processes available.

